<?php

namespace App\DataFixtures;

use App\Entity\Performance;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use App\Repository\UserRepository;
use Faker\Factory;

class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $generator = Factory::create('fr_FR');
        $kevin = new User("Kévin", "Loiseau", "kevinpellin521@gmail.com");
        $manager->persist($kevin);

        for ($i=0; $i<10; ++$i){
            $user = new User($generator->firstName, $generator->lastName, "kevinpellin521@gmail.com");
            $manager->persist($user);
        }

        for($i=0; $i<50; ++$i){
            $performance = new Performance();
            $performance->setName("Prestation ".strval($i+1));
            $performance->setPrice(rand(0.0, 999.50));
            $manager->persist($performance);
        }

        $manager->flush();
    }
}
